
# Laboratorio de kubernetes onpremise

Este es un proyecto técnico sobre la implementación del cluster Kubernetes oprimiste, que es una implementación completa de la plataforma distribuida Kubernetes para administradores de sistemas y desarrolladores.

Este laboratorio será un lugar para mostrar mis conocimientos y compartirlos con la comunidad. Espero aprender de los demás y recibir valiosos comentarios que me ayuden a mejorar los conocimientos generales de todos los participantes.

## Temas que se cubriran

* Diseno de arquitectura de laboratorio
* Ambiente de virtualizacion local
* Creacion de servidores virtuales
* Instalacionn de sistema operativo basico para los servidores virtuales
* Implementacion de un DNS interno
* Implementacion de un servidor de genracion de certificados
* Instalacio de componentes de docker 
* Instalacion de kubernetes basado en la distribucion k3s
* Instalacion de un servicio de registry privado
* Instalacion de de una plataforma de monitoreo y administracion de k8s
* Implementacion y despliegue automatico de aplicaciones

```mermaid
  graph TD;
      N1-->N2;
      N2-->N3;
      N3-->N4;
      N4-->N1;
```

### Nota: 
La implementacion de este laboratorio es para fines educativos por lo cual no se recomienda su implementacion en un ambiente de produccion

Gustavo A. Zuniga G. – gustavozunigagoni@gmail.com
Distributed under the MIT license. See ``LICENSE`` for more information.
